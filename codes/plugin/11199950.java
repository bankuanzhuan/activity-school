/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for (int m=0 ; m<n-1 ;m++){
        for (int q=0 ; q<n-1-m ;q++){
            if (a[q]>a[q+1]) {
                int temp=a[q];
                a[q]=a[q+1];
                a[q+1]=temp;
            }
       }
    }
} //end