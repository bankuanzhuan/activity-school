/**
* 冒泡排序函数
* aa bb cc
* @param a 待排序的数组
* @param n 待排序的数组长度
*/
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for (int i = 0;i < n - 1;i++) {
        for (int j = 0;j < n - i - 1;j++) {
            if (a[j] > a[j + 1]) {
                int temp1 = a[j];
                int temp2 = a[j + 1];
                a[j] = temp2;
                a[j + 1] = temp1;
            }
        }
    }
    for (int m:a) {
        System.out.println(m);
    }
} //end
