/**
 * 冒泡排序函数
 * 冒泡排序的基本原理是通过相邻元素之间的比较和交换，使得较大的元素逐渐从底部浮出到顶部，从而实现排序。
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制比较轮数
    for (int i = 0; i < n - 1; i++) {
        // 内层循环进行相邻元素的比较和交换
        for (int j = 0; j < n - 1 - i; j++) {
            // 如果前面的元素大于后面的元素，则交换它们
            if (a[j] > a[j + 1]) {
                // 使用临时变量进行交换
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} //end

